## List comprehensions en Python

Las [list comprehensions](https://docs.python.org/2/tutorial/datastructures.html#list-comprehensions) en Python son una manera elegante de construir una lista sin tener que usar diferentes iteraciones `for` para agregar valores de uno en uno.

Un ejemplo de list comprehensions es el siguiente:

```
"""Usando for loop"""
>>> values = [7, 8, 9, 23]
>>> num = 8
>>> sum = []
>>> 
>>> for value in values:
...     sum.append(value + num)
... 
>>> print(sum)
[15, 16, 17, 31]
```
Utilizando list comprehensions:

```
"""Usando list comprehension"""
>>> values = [7, 8, 9, 23]
>>> num = 8
>>> 
>>> sum = [value + num for value in values]
>>> print(sum)
[15, 16, 17, 31]
```

#### Nested List Comprehensions

Un ejemplo de nested list comprehensions es el siguiente:

```
"""Usando for loops"""
>>> values = [2, 5]
>>> ranges = [2, 3]
>>> sum = []
>>> for value in values:
...     for x in ranges:
...         sum.append(value + x)
... 
>>> print(sum)
[4, 5, 7, 8]
```

Utilizando list comprehensions:

```
"""Nested List Comprehension"""
>>> values = [2, 5]
>>> ranges = [2, 3]
>>> sum = [value + x for value in values for x in ranges]
>>> print(sum)
[4, 5, 7, 8]
```

## Ejercicio - Maxima diferencia entre elementos de lista con list comprehensions

Es importante realizar este ejercicio usando `list comprehensions`, revisa el código del ejercicio `maxima diferencia entre elementos de lista` realizado anteriormente y utilizalo como referencia. 

Define la función `maximum_difference` que recibe como argumento una lista de números y retorna la maxima diferencia de esa lista. Los resultados de las comparaciones en el `driver code` deben ser `true`.

Un ejemplo para obtener la maxima diferencia de una lista es el siguiente:

Dada la siguiente lista [1, 2, 5], las operaciones para obtener la máxima diferencia serán:

- Primera resta con valor absoluto:

  |1-2| = 1 

- Segunda resta con valor absoluto:

  |1-5| = 4

- Tercer resta con valor absoluto:

  |2-5| = 3

La maxima diferencia es 4.

```python
"""maximum difference function"""

def maximum_difference(list_of_numbers):
    pass



#driver code
print(maximum_difference([1, 2, 5, 0]) == 5)
print(maximum_difference([1, 2, 5]) == 4)
```

